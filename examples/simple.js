(async () => {

const os = require('os')
const process = require('process')
const elwt = require('../.')

let poolInstance = await elwt.Pool.spawnPool()
let tasks = []
for (let i = 0; i < os.cpus().length; i++) {
  let outsidePromise = poolInstance.exec(async _i => {
    console.log(`RUNNING TASK #${_i}`)
    let accum = Math.random()
    for (let j = 0; j < 100000; j++) {
      accum = (q => q * Math.random())(Math.random())
    }
    console.log(`TASK #${_i} DONE!`)
    return accum
  }, i)
  tasks.push(outsidePromise)
};
await Promise.all(tasks)
console.log('ALL TASKS DONE!')
process.exit()

})()
