(async () => {

const elwt = require('../.')
const process = require('process')
const N = process.argv.length > 2 ? process.argv[2] : 100
const M = process.argv.length > 3 ? process.argv[3] : 10000000

console.log(`NON-PARALLEL MUL RUNNING WITH N=${N} == argv[2]||100 AND M=${M} == argv[3]||10000000`)
console.time('NON-PARALLEL MUL RUNNING')
let pss1 = []
for (let k = 0; k < N; k++) {
  let pending = (async input => {
    let accum = Math.random()
    for (let i = 0; i < input.M; i++) accum = Math.random() * Math.random() + input.k
    return accum
  })({ k, M })
  pss1.push(pending)
};
await Promise.all(pss1)
console.timeEnd('NON-PARALLEL MUL RUNNING')

let poolInstance = await elwt.Pool.spawnPool({ size: 2 })
console.log(`PARALLEL MUL RUNNING WITH N=${N} AND M=${M} ...`)
console.time('PARALLEL MUL RUNNING')
let pss2 = []
for (let k = 0; k < N; k++) {
  let pending = poolInstance.exec(async input => {
    let accum = Math.random()
    for (let i = 0; i < input.M; i++) accum = Math.random() * Math.random() + input.k
    return accum
  }, { k, M })
  pss2.push(pending)
};
await Promise.all(pss2)
console.timeEnd('PARALLEL MUL RUNNING')
process.exit()

})()
