(async () => {

const elwt = require('../.')
let shared = new SharedArrayBuffer(2 * Int32Array.BYTES_PER_ELEMENT);
let swarm = await elwt.Pool.spawnPool({ size: require('os').cpus().length })

await swarm.exec(async (input) => {
    let view = new Int32Array(input);
    view[0] = 21;
}, shared);
await swarm.exec(async (input) => {
    let view = new Int32Array(input);
    view[1] = view[0] * 2;
}, shared);
let response = await swarm.exec(async (input) => {
    let view = new Int32Array(input);
    return view[1];
}, shared);
console.log(response);
process.exit()

})()
