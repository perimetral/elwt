const surrial = require('surrial')

const actions = {
	RUN: '__RUN',
	ERROR: '__ERROR',
	DONE: '__DONE',
  FREE: '__FREE',
  WORKER_ONLINE: '__WONLINE',
  WORKER_ERROR: '__WERROR',
  WORKER_TERMINATED: '__WEXIT',
};
actions.raw = Object.values(actions);

async function moveSharedToRaw (input, raw, keyPrefix = '') {
  if (typeof input === 'object') {
    for (let key in input) {
      let item = input[key];
      if (item instanceof SharedArrayBuffer) {
        raw[`${keyPrefix}${key}`] = item;
        delete input[key];
      } else if (typeof item === 'object') {
        await moveSharedToRaw(item, raw, `${key}/${keyPrefix}`);
      };
    };
  };
};

function roundRobin (iterable) {
  return {
    next: function () {
      let original = this._entries.next();
      if (! original.done) return original;
      this._entries = iterable.entries();
      return this._entries.next();
    },
    _entries: iterable.entries(),
  };
};

const serialize = async (...input) => surrial.serialize(...input)
const deserialize = async (...input) => surrial.deserialize(...input)

module.exports = {
  actions,
  moveSharedToRaw,
  roundRobin,
  serialize,
  deserialize,
}
