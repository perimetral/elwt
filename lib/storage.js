class Storage extends Map {
  constructor (...props) {
    super(...props)
  }
  async clear (...data) {
    return super.clear(...data)
  }
  async delete (...data) {
    return super.delete(...data)
  }
  async has (...data) {
    return super.has(...data)
  }
  async set (...data) {
    return super.set(...data)
  }
};

module.exports = Storage
