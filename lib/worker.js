const { Worker } = require('worker_threads')
const defaultTemplater = require('./templater')

class PoolWorker extends Worker {
  constructor ({ template = defaultTemplater(), props = {} } = {}) {
    super(template, {
      ...props,
      eval: true,
    });
  }
};

module.exports = PoolWorker;
