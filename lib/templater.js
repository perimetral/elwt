const { actions } = require('./tools');

module.exports = () => {
  return `
    const { parentPort } = require('worker_threads')
    const surrial = require('surrial')

    const serialize = async (...data) => surrial.serialize(...data)
    const deserialize = async (...data) => surrial.deserialize(...data)
    parentPort.on('message', async ({ action, port, fn, data, raw }) => {
      if (action !== '${actions.RUN}') return;
      try {
        let cleanData = data && (data instanceof SharedArrayBuffer ? data : Object.assign(await deserialize(data), raw));
        let dfn = await deserialize(fn);
        port.postMessage({ action: '${actions.DONE}', result: await serialize(await dfn(cleanData)) });
        port.close();
      } catch (e) {
        port.postMessage({ action: '${actions.ERROR}', result: await serialize(e), error: true });
        port.close();
      };
    });
  `;
};
